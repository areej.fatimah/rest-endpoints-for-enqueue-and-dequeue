package com.example.demo.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
@Configuration
public class JmsConfig {
	String BROKER_URL = "tcp://localhost:61616";
	String BROKER_USERNAME = "admin"; 
	String BROKER_PASSWORD = "admin";
	String queue="test-queue";
	
	@Bean
	public ActiveMQConnectionFactory connectionFactory(String url){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    connectionFactory.setBrokerURL(url);
	    connectionFactory.setPassword(BROKER_USERNAME);
	    connectionFactory.setUserName(BROKER_PASSWORD);
	    return connectionFactory;
	}
	@Bean
	public Session session(String url) throws JMSException {
		Connection connection = connectionFactory(url).createConnection();
		Session sesion = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		connection.start();
		return sesion;
	}
	@Bean
	public MessageProducer messageProducer() throws JMSException {
		Destination destination = session(BROKER_URL).createQueue(queue);
		MessageProducer producer = session(BROKER_URL).createProducer(destination);
		return producer;
	}
	@Bean
	public MessageConsumer messageConsumer() throws JMSException{
		Destination destination = session(BROKER_URL).createQueue(queue);
		MessageConsumer messageConsumer = session(BROKER_URL).createConsumer(destination);
		return messageConsumer;
	}
}