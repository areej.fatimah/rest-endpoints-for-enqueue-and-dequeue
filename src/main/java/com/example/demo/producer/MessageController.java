
package com.example.demo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

@RestController
@RequestMapping("/api")
public class MessageController {
	
	@Autowired
	Session session;
	@Autowired
	MessageProducer messageProducer;
	
	@PostMapping("message/{message}")
    public String send(@PathVariable("message")String message) throws JMSException{
	    TextMessage textMessage = session.createTextMessage(message);
	    messageProducer.send(textMessage);
	    return "OK";
    }
}
